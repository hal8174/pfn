#!/bin/bash

for file in $(find . -type f -name Makefile)
do
    make -C $(dirname $file) clean
done