# Antworten auf die Probeklausur

1.  In der **Objektorientierten** Programmierung werden *Objekte* die zu einer **Klasse** gehören zur Strukturierung von *Daten* benutzt. Für eine Klasse werden auch *Methoden* (das gleiche wie Funktionen) definiert dessen Aufgabe ist es *Objekte* zu manipulieren.
2.  ```python
    3
    4
    2
    ```
3.  ```python
    1    c
    2    b
    3    a
    ```
4. 
| Befehl       | Aussage                                                                                                                  |
| :----------- | :----------------------------------------------------------------------------------------------------------------------- |
| `dict()`     | erzeuge ein neues leeres Dictionary                                                                                      |
| `d[k]`       | liefere den Wert für Schlüssel `k` im Dictionary `d`                                                                     |
| `if k in d:` | Teste, ob der Schlüssel `k` im Dictionary `d` vorkommt                                                                   |
| `d[k] = v`   | füge Schlüssel/Wertpaar `k/v` in das Dictionary `d` ein; falls Schlüssel `k` existiert, überschreibe den bisherigen Wert |
| `d.keys()`   | liefere Liste der Schlüssel des Dictionaries `d` in beliebiger Ordnung                                                   |
| `d.values()` | liefere Liste der Werte des Dictionaries `d` in beliebiger Ordnnung                                                      |
| `d.items()`  | liefere Liste der Schlüssel/Wertepaare des Dictionaries `d` in beliebiger Reihenfolge                                    |
5.  ```python
    def change_first_leter_sub(w):
        return w[0] + re.sub(w[0], '\$', w[1:])
    ```
6.  ```python
    def np_symmetric(m):
        for i in range(len(m)):
            for j in range(i + 1):
                if m[i, j] != m[j, i]:
                    return False
        return True
    ```
7.  ```python
    def __eq__(self, other):
        return (self.num == other.num) and (self.den == other.den)
    ```
8.  Da in den **Reelen** Zahle auch **Irrationale** Zahlen, die man nie genau *speichern* kann, enthalten sind und da *Speicher* und *Rechenkapazität* begrenzt sind, muss man einen Kompromiss finden. Diese sind die **Fließkommazahlen**. **Fließkommazahlen** sind klein, da sie nur den relevantesten Teil einer Zahl *speichern* (vorstellbar als eine feste Anzahl an signifikanten Stellen). Außerdem können Rechner mit **Fließkommazahlen** relativ gut *rechnen*.
9.  ```python
    def f(x, stddev):
        return 2/(np.sqrt(3*stddev)*np.pi**(1/4))*(1-(x/stddev)**2)*np.exp(-1*x**2/(2*stddev**2))

    def plot_mexican_hat_func(stddev_list, min_val, max_val, n):
        x = np.linspace(min_val, max_val, n)

        fig, ax = plt.

        for stddev in stddev_list:
            ax.plot(x, f(x, stddev))

        fig.savefig('maxican_hat.pdf')
    ```
10. ```python
    def gen_fac_series():
        i = 1
        n = 1
        while True:
            yield i
            i *= n
            n += 1
    ```
11. ```python
    @app.route(’/<expression>’)
    def calc(expression):
    ```