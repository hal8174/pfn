
# Bearbeitungszeit: 1.2 Stunden

from math import sqrt


def split_number_enumerate(number, terms_of_sum):

    stack = [terms_of_sum[0]]

    while len(stack) > 0:

        if sum(stack) < number:
            stack.append(stack[-1])

        else:

            if sum(stack) == number:
                yield stack.copy()

            while len(stack) > 0 and stack[-1] == terms_of_sum[-1]:
                stack.pop()

            if len(stack) > 0:
                element = stack.pop()
                i = terms_of_sum.index(element)
                stack.append(terms_of_sum[i + 1])


def quality_function(n):

    s = 0

    mean = sum(n) / len(n)

    for i in n:
        s += (i - mean)**2

    return sqrt(s)


def split_number_itrv(number, terms_of_sum):

    bestsplit = [None, None]

    for solution in split_number_enumerate(number, terms_of_sum):

        if bestsplit[1] is None or bestsplit[1] > quality_function(solution):
            bestsplit = [solution, quality_function(solution)]

    return bestsplit
