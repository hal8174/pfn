
# Bearbeitungszeit: 0.33 Stunden


class Atom:
    def __init__(self, ident, name, x, y, z, atomtype, optional_list):
        self.ident = ident
        self.name = name
        self.x = x
        self.y = y
        self.z = z
        self.atomtype = atomtype
        self.optional_list = optional_list
        return

    def __str__(self):
        res = '{} {} {} {} {} {}'.format(self.ident, self.name, self.x, self.y,
                    self.z, self.atomtype)
        return res + ' '.join(self.optional_list)

    def __sub__(self, other):  # required only for LJ-Potential
        # TODO
        return


class Bond:
    def __init__(self, ident, atomid1, atomid2, kind, optional_list):
        self.ident = ident
        self.atomid1 = atomid1
        self.atomid2 = atomid2
        self.kind = kind
        self.optional_list = optional_list
        return

    def atomid1(self):
        return self._atomid1

    def atomid2(self):
        return self._atomid2

    def __str__(self):
        res = '{} {} {} {}'.format(self.ident, self.atomid1, self.atomid2,
                    self.kind)
        return res + ' '.join(self.optional_list)


class Molecule:
    def __init__(self, molecule_name, atom_list, bond_list):
        self.name = molecule_name
        self.atom_list = []
        for atom in atom_list:
            self.atom_list.append(Atom(atom[0], atom[1], atom[2], atom[3],
                        atom[4], atom[5], atom[6:]))
        self.bond_list = []
        for bond in bond_list:
            self.bond_list.append(Bond(bond[0], bond[1], bond[2], bond[3],
                        bond[4:]))
        return

    def name(self):
        return self.name

    def atoms_number(self):
        return self.atom_list

    def atom_list(self):
        return self.atom_list

    def bonds_number(self):
        return len(self.bond_list)

    def __str__(self):
        res = ['@<TRIPOS>MOLECULE']
        res.append(self.name)
        res.append('@<TRIPOS>ATOM')
        for atom in self.atom_list:
            res.append(str(atom))
        res.append('@<TRIPOS>BOND')
        for bond in self.bond_list:
            res.append(str(bond))
        return '\n'.join(res)

    def __sub__(self, other):  # required only for LJ-Potential
        # TODO
        return

    def adjacency_matrix(self):  # needed for shortest path algorithm
        # TODO
        return
