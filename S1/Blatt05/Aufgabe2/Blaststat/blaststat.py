#!/usr/bin/env python3

# Bearbeitungszeit: 1 Stunden

import sys, re
from math import log10


if len(sys.argv) != 2:
  sys.stderr.write('Usage: {} <inputfile>\n'.format(sys.argv[0]))
  exit(1)

inputfile = sys.argv[1]
try:
    stream = open(inputfile)
except IOError as err:
    sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
    exit(1)

bitscores_dict = dict()
expect_dict = dict()
ident_dict = dict()

for line in stream:
    f = re.search(r'Score = (\d+\.?\d*) bits.*Expect = (0\.0|\de-\d+)',
            line)
    
    if f:
        x = round(float(f.group(1)))

        if not x in bitscores_dict:
            bitscores_dict[x] = 0
        bitscores_dict[x] += 1

        x = float(f.group(2))
        if x == 0:
            x = 0
        else:
            x = round(log10(x))

        if not x in expect_dict:
            expect_dict[x] = 0
        expect_dict[x] += 1

    g = re.search(r'Identities = \d+/\d+ \((\d+)%\)', line)

    if g:
        x = int(g.group(1))

        if not x in ident_dict:
            ident_dict[x] = 0
        ident_dict[x] += 1

print('# distribution of bitscores')
print('# Fields: bitscore, occurrences')
for k in sorted(bitscores_dict):
    print('{}\t{}'.format(k, bitscores_dict[k]))


print('# distribution of identities')
print('# Fields: identity, occurrences')
for k in sorted(ident_dict):
    print('{}\t{}'.format(k, ident_dict[k]))


print('# distribution of log10(expect)')
print('# Fields: log10(expect), occurrences')
for k in sorted(expect_dict):
    print('{}\t{}'.format(k, expect_dict[k]))
