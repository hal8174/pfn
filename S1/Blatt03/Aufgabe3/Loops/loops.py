#!/usr/bin/env python3

# Bearbeitungszeit: 0.3 Stunden

n = 10

print("==== 1 =====")
# [1] convert to while loop:
# for i in range(1, n, 3):
#  print(i)

i = 1
while i < 10:
    print(i)
    i += 3

# [2] convert to for loop:
print("==== 2 =====")
#i = 0
#j = n
# while i < j:
#  print(i, j)
#  i += 1
#  j -= 1

for i in range(n//2):
    print(i, n-i)

# [3] convert to while loop:
print("==== 3 =====")
# for i in range(n, -n, -1):
#  print(i)

i = n
while i > -n:
    print(i)
    i -= 1

# [4] convert to for loop:
print("==== 4 =====")
#i = 1
# while True:
#  print(i)
#  i += 1
#  if i > n:
#    break

for i in range(1, n+1):
    print(i)
