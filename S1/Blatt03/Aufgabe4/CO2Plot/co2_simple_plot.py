#!/usr/bin/env python3

# Bearbeitungszeit: 1.2 Stunden

import matplotlib.pyplot as plt
import sys
plt.switch_backend('agg')  # to allow remote use

jahre = [1971, 1975, 1980, 1985, 1990, 1995, 2000, 2005, 2010, 2016]

laender = []
data_lists = []

try:
    stream = open('data.tsv', 'r')
except IOError as err:

    sys.stderr.write('{}: {}\n'.format(sys.argv[0], err))
    exit(1)


for line in stream:

    str_list = line.rstrip().split('\t')

    laender.append(str_list[0])

    float_list = []

    for number in str_list[1:]:
        float_list.append(float(number))

    data_lists.append(float_list)

stream.close()

fig, ax = plt.subplots(figsize=(10, 6.18))

ax.set_title('Entwicklung der $CO_2$ Emissionen fuer einige Industrielaender')

ax.set_ylabel('$CO_2$ Emissionen/Person (t)')
ax.set_xlabel('Jahre')

ax.set_xticks(jahre)

for i in range(len(laender)):

    ax.plot(jahre, data_lists[i], label=laender[i])

ax.legend(loc='upper right')

plt.savefig('CO2_plot.pdf')
