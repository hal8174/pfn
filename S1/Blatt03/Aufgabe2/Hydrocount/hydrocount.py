#!/usr/bin/env python3

# Bearbeitungszeit: 0.3 Stunden

import sys

hydrophobalph = ['A', 'F', 'I', 'L', 'M', 'P', 'V', 'W']
hydrophob = 0
hydrophilalph = ['C', 'D', 'E', 'G', 'H', 'K', 'N', 'Q', 'R', 'S', 'T', 'Y']
hydrophil = 0

if len(sys.argv) != 2:
    sys.stderr.write('Usage: {} <aminoacid sequence>\n'.format(sys.argv[0]))
    exit(1)

for i in sys.argv[1]:
    if i in hydrophobalph:
        hydrophob += 1
    elif i in hydrophilalph:
        hydrophil += 1

print('hydrophobic={}, hydrophilic={}'.format(hydrophob, hydrophil))
