#!/usr/bin/env python3
#Bearbeitungszeit: 1.2 Stunden

import sys, argparse
from operator import itemgetter

def parse_command_line():
    p = argparse.ArgumentParser()
    p.add_argument('-g', '--gold_standard', type=str)
    p.add_argument('input_file', type=str, nargs='+')
    return p.parse_args()

args = parse_command_line()

def se(p, g):

    return 100 * tp(p, g) / len(g)


def sp(p, g):

    return 100 * tp(p, g) / len(p)

def tp(p, g):

    tp = p & g

    return len(tp)

try:
    stream = open(args.gold_standard)
except IOError as err:
    sys.sdterr.write('{}: {}'.format(sys.argv[0], err))
    exit(0)

goldstandard = set()

for line in stream:
    goldstandard.add(tupel(line.rstrip().split('\t')))

stream.close()

print('# Fields: filename\ttp\tsens\tspec\thmean')

result = list()

for filename in args.input_file:

    try:
        stream = open(filename)
    except IOError as err:
        sys.sdterr.write('{}: {}'.format(filename, err))
        exit(0)

    current = set()

    for line in stream:
        current.add(tupel(line.rstrip().split('\t')))

    stream.close()

    result.append({'filename': filename,
                        'tp': tp(current, goldstandard),
                        'se': se(current, goldstandard),
                        'sp': sp(current, goldstandard),
                        'hmean': 2 / (1 / se(current, goldstandard)
                                    + 1 / sp(current, goldstandard))})


for value in sorted(result, key=itemgetter('hmean')):

    print('{}\t{:6}\t{:6.2f}\t{:6.2f}\t{:6.2f}'.format(value['filename'],
                    value['tp'], value['se'], value['sp'], value['hmean']))


