#!/usr/bin/env python3
#Bearbeitungszeit: 1.0 Stunden

from math import sqrt

def quality_function(n):

    s = 0

    mean = sum(n) / len(n)

    for i in n:
        s += (i - mean)**2

    return sqrt(s)

def split_number_rec(terms_of_sum, best_split, remain, terms_idx, l):

    for idx, term in enumerate(terms_of_sum[terms_idx:]):

        new_l  = l.copy()
        new_l.append(term)

        if remain - term > 0:

            split_number_rec(terms_of_sum, best_split, remain - term,
                        terms_idx + idx, new_l)

        elif remain - term == 0:

            quality = quality_function(new_l)

            if best_split[1] is None or quality < best_split[1]:
                best_split[0] = new_l
                best_split[1] = quality


def split_number(number, terms_of_sum):

    best_split = [None, None]
    l = list()

    split_number_rec(terms_of_sum, best_split, number, 0, l)

    return best_split

