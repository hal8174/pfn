#!/usr/bin/env python3

# Bearbeitungszeit: 5 Stunden

from bs4 import BeautifulSoup
import re
from data_matrix_class import DataMatrix
import matplotlib.pyplot as plt
from float_or_None import float_or_None
from country2region import Country2Region


class CO2Stat:

    def __init__(self, co2_stat_table_lines):
        self._dm = DataMatrix(co2_stat_table_lines, 0, '\t', True)

        self._year_list = self._dm.attribute_list()[2:]

        self._all_countries = list()
        for country in self._dm.keys():
            if self._dm[country]['is_country'] == '1':
                self._all_countries.append(country)

    def plot_for_country_list(self, country_list, color_list, groupname,
                              prefix):

        fig, ax = plt.subplots()

        ax.set_title('$CO_2$ emissiones for some ' + groupname + 'countries')
        ax.set_xlabel('years')
        ax.set_ylabel('$CO_2$ emission per person (tons)')

        for country, color in zip(country_list, color_list):
            data = list()
            for year in self._year_list:
                data.append(float_or_None(self._dm[country][year]))
                # __getitem__
            ax.plot(self._year_list, data, color=color, label=country)

        ax.legend(loc='best', fontsize='small')

        fig.savefig(prefix + '.pdf')

    def histogram_all_emissions(self, prefix):

        fig, ax = plt.subplots()

        ax.set_xlabel('emissions per person for all countries (tons)')
        ax.set_ylabel('number of events')
        ax.set_title('distribution of $CO_2$ emission values')
        ax.set_xlim(0, 20)

        values = list()

        for country in self._all_countries:
            for years in self._year_list:
                if not float_or_None(self._dm[country][years]) is None:
                    values.append(float_or_None(self._dm[country][years]))

        ax.hist(values, bins=750)

        fig.savefig('histogram.pdf')

    def groupby(self, select_func):

        groups = dict()
        for country in self._all_countries:
            g = select_func(country)
            if g is not None:
                if g not in groups:
                    groups[g] = list()

                for year in self._year_list:
                    v = float_or_None(self._dm[country][year])
                    if v is not None:
                        groups[g].append(v)
        return groups

    def boxplot_by_unit(self, for_continent, prefix):

        fig, ax = plt.subplots()

        ax.set_title('distribution of $CO_2$ emissions values by continent')
        ax.set_ylabel('$CO_2$ emission per persons (tons)')

        c2r = Country2Region()

        if for_continent:
            groups = self.groupby(c2r.continent)
        else:
            groups = self.groupby(c2r.region)

        ax.boxplot(groups.values(), labels=groups.keys())
        ax.set_xticklabels(groups, rotation=25, ha='right')

        fig.savefig(prefix + '.pdf')

    def show_orig(self):
        attributes = self._dm.attribute_list()
        keys = self._dm.keys()
        return self._dm.show_orig('\t', attributes, keys)


def convert_table_headers(h_list):
    h_list = h_list[1:]
    century = ''
    for i in range(len(h_list)):
        if len(h_list[i]) == 4:
            century = h_list[i][0:2]
        elif len(h_list[i]) == 3:
            h_list[i] = century + h_list[i][1:]
    return ['Staat/Region', 'is_country'] + h_list


def co2_stat_table_lines_get(co2_stat_html):
    soup = BeautifulSoup(co2_stat_html, features='html.parser')
    table = soup.find('table')
    assert table
    thead = table.find('thead')
    tbody = table.find('tbody')
    thead = [x.text for x in thead.find_all('th')]
    data = ['\t'.join(convert_table_headers(thead))]
    for row in tbody.find_all('tr'):
        data_fields = [td for td in row.find_all('td')]
        assert len(data_fields) == 12
        r = re.search(r'</span>([\w\W]+)</td>', str(data_fields[0]))
        country = r.group(1).strip()
        r = re.search(r'<b>(.+)</b>', country)
        if r:
            is_country = '0'
            country = r.group(1).strip()
        else:
            is_country = '1'
        r = re.search(r'<em>(.+)</em>', country)
        if r:
            country = r.group(1).strip()
        data_fields = [td.string.replace(',', '.').strip()
                       for td in data_fields[1:]]
        data.append('\t'.join([country, is_country] + data_fields))
    return data
