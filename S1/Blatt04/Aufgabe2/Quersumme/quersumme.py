#!/usr/bin/env python3
#Bearbeitungszeit: 0.3 Stunden

import sys
import re

if len(sys.argv) != 2:
    sys.stderr.write('Usage: {} <integer>\n'.format(sys.argv[0]))
    exit(1)

if not re.search(r'^[+-]?\d+$', sys.argv[1].strip()):
    sys.stderr.write('{}: argument "{}" is not an integer'.format(\
            sys.argv[0], sys.argv[1]))
    exit(1)

numbers = '1234567890'

quersumme = 0

for digit in sys.argv[1]:
    if digit in numbers:
        quersumme += int(digit)

print('{}\t{}'.format(sys.argv[1].strip(), quersumme))
