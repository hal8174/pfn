#!/usr/bin/env python3
#Bearbeitungszeit: 0.5 Stunden

import sys
import re

# Fehler Abfangen
if len(sys.argv) != 2:
    sys.stderr.write(\
            'Usage: {} <inputfile with dates in format dd.mm.yyyy>\n'\
            .format(sys.argv[0]))
    exit(1)

try:
    stream = open(sys.argv[1])
except IOError as err:
    sys.stderr.write('{}: {}\n'.format(sys.argv[0], err))
    exit(1)

dayInMonths =           [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30]
dayInMonthsSchaltjahr = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30]


# Umrechnen
for line in stream:
    date = line.strip()

    m = re.search(r'(\d{2}).(\d{2}).(\d{4})', date)

    year = int(m.group(3))
    month = int(m.group(2))
    day = int(m.group(1))

    total = 0
    for i in range(month - 1):
        if year % 400 == 0 or (year % 4 == 0 and year % 100 != 0):
            total += dayInMonthsSchaltjahr[i]
        else:
            total += dayInMonths[i]

    total += day

    print('{}\t{}'.format(date, total))
