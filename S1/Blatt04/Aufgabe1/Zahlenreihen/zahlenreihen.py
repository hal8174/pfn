#!/usr/bin/env python3
#Bearbeitungszeit: 1 Stunden

import sys


# Fehler abfangen
if len(sys.argv) != 2:
    sys.stderr.write('Usage: {} <k>\n'.format(sys.argv[0]))
    exit(1)

try:
    k = int(sys.argv[1])
except ValueError as err:
    sys.stderr.write("{}: cannot convert '{}' to int\n".format(\
            sys.argv[0], sys.argv[1]))
    exit(1)

if k <= 0:
    sys.stderr.write('{}: parameter {} is not positive int\n'.format(\
            sys.argv[0], sys.argv[1]))
    exit(1)


# Reihe a
print('Reihe a')
total = 0
x = 0
for i in range(1, k + 1):
    x += 2
    total += x
    print(x)

print('Summe: {}'.format(total))


# Reihe b
print('Reihe b')
total = 0
num = 3 #numerator
den = 1 #denominator
for i in range(1, k + 1):
    num += 2
    den *= 2
    total += num / den
    print('{:.5e}'.format(num / den))

print('Summe: {:.5e}'.format(total))


# Reihe c
print('Reihe c')
total = 0
x = -2
for i in range(1, k + 1):
    x /= -2
    total += x
    print('{:.5e}'.format(x))

print('Summe: {:.5e}'.format(total))


# Reihe d
print('Reihe d')
total = 0
x = 1
for i in range(1, k + 1):
    x *= i
    total += 1 / x
    print('{:.5e}'.format(1 / x))

print('Summe: {:.5e}'.format(total))
