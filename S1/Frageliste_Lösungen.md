# Antworten auf die Frageliste

1.  `#!/usr/bin/env python3`
2.  Der Python Interpreter führt datein aus und in der Python shell kenn der Nutzer zeilenweise Python code ausführen.
3.  a)  `ls` ist eine Liste in der sich weitere Listen befinden.
    
    b)  `'b'` ist der Character b als String.

    c)  `1` ist ein Integer.

    d)  `['a', 1]` ist eine Liste in der sich ein String und ein Integer befinden.

    e)  `[['a', 1], ['c', 2]]` ist eine Liste in der sich weitere Listen befinden.

4.  In der **Objektorientierten** Programmierung werden *Objekte* die zu einer **Klasse** gehören zur Strukturierung von *Daten* benutzt. Für eine Klasse werden auch *Methoden* (das gleiche wie Funktionen) definiert dessen Aufgabe ist es *Objekte* zu manipulieren.
5.  `[chr(ord('a') + i) for i in range(26)]`
6.  `__init__`
7.  Strings
8.  ```python
    def two_from_both_ends(w):
        if len(w) < 2:
            return ''
        return w[:2] + w[-2:]
    ```
9.  Coole Variante:
    ```python
    def select_at_even_indices(w):
        return w[::2]
    ```
    Variante mit List-Comprehensions:
    ```python
    def select_at_even_indices(w):
        return ''.join([w[i] for i in range(len(w)) if i % 2 == 0])
    ```
10. ```python
    n = 10
    for i in range(0, n // 2):
        print(i, n - i)
    ```
11. ```python
    n = 10
    i = 0
    while i < n // 2:
        print(i, n - 1)
        i += 1
    ```
12. `NM_0016`
13. `3`
14. `[True, False, True]`
15. ```
    0
    0.5
    ```
16. String Methoden
    
|         Methoden          | Aufgabe                                                                             |
| :-----------------------: | :---------------------------------------------------------------------------------- |
|         `len(s)`          | bestimme die Anzahl der Elemente im String `s`                                      |
|         `s[i:j]`          | liefere Substring von String `s` von Index `i` bis `j - 1`                          |
|      `s.split(sep)`       | Spalte einen String `s` am Seperator `sep` in eine Lise von Strings                 |
|       `s.rstrip()`        | lösche Leerzeichen am Ende von String                                               |
|       `s.lstrip()`        | lösche Leerzeichen am Anfang von String                                             |
|        `s.strip()`        | lösche Leerzeichen am Anfang und Ende von String                                    |
|  `text.replace(s, '{}')`  | Substituire den String `s` für den Platzhalter `{}`                                 |
| `t = str.maketrans(x, y)` | erzeuge Übersetzungstabelle, die die Elemente von `x` auf Elemente von `y` abbildet |
|     `s.translate(t)`      | Wende die Übersetzungstabelle `t` auf den String `s` an                             |

17. ```python
    st = s + t
    st = ''.join([s, t])
    ```
18. ```python
    s = re.sub(r'\d', '', s)
    ```
19. ```python
    def avg_from_file(filename):
        count = 0
        summe = 0
        for line in open(filename, 'r'):
            summe += int(line.strip())
            count += 1
        return summe / count
    ```
20. `sys.argv[0]` dazu muss man das `sys` modul importieren
21. ```python
def myint(s):
    summe = 0
    for c in s:
        summe *= 10
        summe += ord(c) - ord('0')
    return summe
    ```
22. 
| Regexp    | Bedeutung                                         |
| :-------- | :------------------------------------------------ |
| `[abc]`   | Die Buchstaben `a`, `b` und `c`                   |
| `[^abc]`  | Alles auser die Buchstaben `a`, `b` und `c`       |
| `[a-z]`   | Alle buchstaben des Alphabetes in klein.          |
| `^`       | Den Beginn des Strings                            |
| `$`       | Das Ende des Strings                              |
| `.`       | Alle charachtere auser newlines.                  |
| `\s`      | Ein whitespace                                    |
| `\d`      | Eine Ziffer                                       |
| `(a|b)`   | Der Character `a` oder `b` wird gematched         |
| `a?`      | Ein oder keinmal der Charakter `a`                |
| `a*`      | Kein oder eine belibige Anzahl des Charakters `a` |
| `a+`      | Mindestens einmal der Charakter `a`               |
| `a{3}`    | Genau dreimal der Charakter `a`                   |
| `a{3, 6}` | Drei bis sechs mal der Charakter `a`              |
23. ```python
    def file2words(filename):
        try:
            stream = open(filename, 'r')
        except IOError as err:
            sys.stderr.write('{}: {}'.format(sys.argv[0], err))
            exit(1)
        words = dict()
        for word in re.findall(r'\b\w+\b', stream.read()):
            if word not in words:
                words[word] = 0
            words[word] += 1
        stream.close()
        return words
    ```
24. ```python
    class MyFloat:
        def __init__(self, number):
            self._number = float(number)
        def __str__(self):
            return '{:6.3f}'.format(self._number)
    ```
25. ```python
    class MyFloat:
        def __init__(self, number):
            self._number = number
        def __eq__(self, other):
            if '{:.2f}'.format(self._number) == '{:.2f}'.format(other._number):
                return True
            else:
                return False
    ```
26. ```python
    def pythagorean_gen(n):
        for i in range(1, n + 1):
            for j in range(i, n + 1):
                if math.sqrt(i**2 + j**2).is_integer():
                    yield (i, j, int(math.sqrt(i**2 + j**2)))
    ```
27. Weil nach dem String `'motif'` gesucht wird anstadt dem was im string motif gespeichert ist.
28. ```python
    try:
        stream = open(filename, 'r')
    except IOError as err:
        sys.stderr.write('{}: {}'.format(sys.argv[0], err))
        exit(1)
    ```
29. ```python
    def date_reformat_g2intl(date):
        r = re.search(r'(\d{2})\.\s*(\w+)\s*(\d{4})', date)
        return '{}-{}-{}'.format(r.group(3), r.group(monate.index(r.group(2)) + 1, r.group(1))
    ```
30. ```
    u=ccg
    v=ccgt
    ```
    Der string wird nicht verändert weil der Funktion der Wert des Strings übergeben wird.
    Die Liste wird der Funktion als Reference übergeben daher verändert sich bei veränderung auch die liste die der Funktion übergben wird.
31. ```python
    def partialsums(numbers):
        summe = 0
        for number in numbers:
            summe += number
            yield summe
    ```
32. ```python
    def rmsd(vvector, wvctor):
        assert(len(vvector) == len(wvector))
        summe = 0
        for v, w in zip(vvector, wvector):
            summe += (v.x - w.x)**2 + (v.y - w.y)**2 + (v.z - w.z)**2
        return math.sqrt(summe/len(vvector))

    ```
33. ```python
    def majority(s):
        chars = dict()
        for char in s:
            if char not in chars:
                chars[char] = 0
            chars[char] += 1
        return max(chars, key=chars.get)

    ```
34. ```python
    class Distribution:
        def __init__(self, dist=dict()):
            self._dist = dist

        def add_single(self, k, v=1):
            if k not in self._dist:
                self._dist[k] = 0
            self._dist[k] += v
        
        def __add__(self, other):
            new_dist = self._dist.copy()
            for k in other._dist:
                if k not in new_dist._dist:
                    new_dist[k] = 0
                new_dist[k] += other._dist[k]
            return Distribution(new_dist)
        
        def __str__(self):
            res = list()
            for k in self._dist:
                res.append('{} {}'.format(k, self._dist[k]))
            return '\n'.join(res)
    ```
35. ```python
    def reverse_complement(text):
        trans = str.maketrans('ACGT', 'TGCA')
        return text.translate(trans).reverse()
    ```
36. ```python
    def mygrep(pattern, filename):
        try:
            stream = open(filename, 'r')
        except IOError as err:
            sys.stderr.write('{}: {}'.format(sys.argv[0], err))
            exit(1)
        for line in stream:
            r = re.search(pattern, line)
            if r:
                print(line, end='')
    ```
37. ```python
    def dna2peptide(sequence):
        return ''.join([codon2aa(sequence[i*3:(i+1)*3]) for i in range(len(sequence)//3)])
    
    ```
38. ```python
    def keyvaluefiles2dict(filenames):
        keyvalue = dict()
        for filename in filenames:
            try:
                stream = open(filename, 'r')
            except IOError as err:
                sys.stderr.write('{}: {}'.format(sys.argv[0], err))
                exit(1)
            for line in stream:
                line = line.strip().split('\t')
                if line[0] in keyvalue:
                    sys.stderr.write('{}: Duplicate key {} found in file {}'.format(sys.argv[0], line[0], filename))
                    exit(1)
                keyvalue[line[0]] = line[1]
        return keyvalue
    ```
39. Wenn man aus den aus der **Datei** *extrahierten* **Daten**, die **Datei** exakt *wiederherstellen* kann kann man sich sicher sein das alle **Daten** *extrahiert* wurden.
40. ```python
    def data_matrix_new(lines):
        data_matrix = dict()
        attributes = None
        for line in lines:
            if not attributes:
                attributes = line.strip().split('\t')
            else:
                temp = dict()
                line = line.strip().split('\t')
                for i in range(len(line)):
                    temp[attributes[i]] = line[i]
                data_matrix[line[1]] = temp
        return attributes, data_matrix
    ```
41. Für das **Parsen** von **Argumenten** muss man definieren welche **Argumente** es gibt. Für jedes definiert man auch ob dies ein *optionales* **Argument** ist. Außerdem kann man definieren welchen Typ das **Argument** hat und ob es mehrere **Parameter** hat. Man kann auch noch eine Hilfe für jedes **Argument** definieren, welche angezeit wird wenn man die *Hilfsfunktion* benutzt.
42. **Datenkapslung** bedeutet das man variablen von **Klassen** versteckt, so das diese nicht direkt erreichbar sind (also mit einem Unterschtrich vor dem *Variablennamen*). Dies ist in *Python* aber nicht wirklich eine *private* **Variable** sondern nur eine Konvention, die Besagt, das der Nutzer die **Variable** nicht benutzen soll, obwohl es möglich wäre.
43. ```python
    def gcd(x, y):
        while x % y != 0:
            tempY = y
            y = x % y
            x = tempY
        return y

    ```
44. ```python
    def __add__(self, other):
        return Fraction(self.num * other.den + other.num + self.den, self.den * other.den)
    ```
45. ```python
    def np_prepare_plot_data(f, x_min, x_max, numpoints):
        x_list = np.linspace(x_min, x_max, numpoints + 1)
        y_list = f(x_list)
        return x_list, y_list
    ```
46. ```python
    def np_approx_integral_trpz(f, p, q, n):
        d = (q - p) / n
        return (np.sum(f(np.linspace(p + d, q - d, n - 1))) + (0.5 * (f(p) + f(q)))) * d 
    ```
47. Da in den **Reelen** Zahle auch **Irrationale** Zahlen, die man nie genau *speichern* kann, enthalten sind und da *Speicher* und *Rechenkapazität* begrenzt sind, muss man einen Kompromiss finden. Diese sind die **Fließkommazahlen**. **Fließkommazahlen** sind klein, da sie nur den relevantesten Teil einer Zahl *speichern* (vorstellbar als eine feste Anzahl an signifikanten Stellen). Außerdem können Rechner mit **Fließkommazahlen** relativ gut *rechnen*.
48. Auf Grund der Tatsache das bei **Fließkommazahlen** nur eine gewisse anzahl an *significanten Stellen* gibt, kann es zu kleinen *Rechenfehlern* kommen. Zum Beispiel ist beim addieren von **Fließkommazahlen** die Reihenfolge wichtig den das Ergebnis kann bei einem Vergleich ungleich sein. Um Probleme damit zu vermeiden kann man bei exakten Zahlen auf andere **Datentypen** zurückgreifen.
49. ```python
    def gensigneddubles(maxabs):
        i = 1
        while abs(i) <= maxabs:
            yield i
            i *= -2
    ```
50. ```python
    def prepare_plot_data(f, x_min, x_max, numpoints):
        stepwidth = (x_max - x_min) / numpoints
        x_list = [x_min + i * stepwidth for i in range(numpoints + 1)]
        y_list = [f(x) for x in x_list]
        return x_list, y_list
    ```
51. ```python
    def molminiIterator(stream):
        molecule = None
        atoms = list()
        for line in stream:
            r = re.match(r'MOLECULE (\w+)', line)
            if r:
                if not molecule:
                    yield (molecule, atoms)
                    molecule = None
                    atoms = list()
                molecule = r.group(1)
            else:
                atoms.append(line.strip())
        if not molecule:
            yield (molecule, atoms)
    ```
52. 
| Numpy                         | Beschreibung                                                                                 |
| :---------------------------- | :------------------------------------------------------------------------------------------- |
| `np.array([1, 2])`            | erzeuge 1-dim. Arrey mit zwei Elementen 1 und 2                                              |
| `np.array([[1, 2], [3, 4]])`  | erzeuge 2-dim Array (Matrix)                                                                 |
| `np.arrange(n)`               | erzeuge 1-dim `int`-Array mit `n` Elementen                                                  |
| `np.linspace(i, j, k)`        | erzeuge 1-dim `float`-Array mit `k` Elementen, die gleichmäßig von `i` bis `j` verteilt sind |
| `np.ones((r, c))`             | erzeuge 2-dim `float`-Array mit `r` Zeilen und `c` Spalten, initialisiert mit `1.0`          |
| `np.zeros((r, c), dtype=int)` | erzeuge 2-dim `int`-Array mit `r` Zeilen und `c` Spalten, initialisiert mit 0                |
| `np.eye(r)`                   | erzeuge 2-dim `float`-Array mit `r` Zeilen und Spalten; Hauptdiagonale ist 1, andere 0       |
| `np.diag(md)`                 | erzeuge 2-dim Array; Haupdiagonale besteht aus Werten in `md`                                |
| `matrix[i][j] = v`            | überschreibe Matrix-Wert in Zeile `i` und Spalte `j` mit `v`                                 |
| `np.tile(rep, n)`             | erzeuge `n` Kopien von `rep`                                                                 |
53.  Das `numpy`-Modul beinhaltet viele Funktionen und Datenstrukturen die sehr nützlich sind um mit Listen und anderen wissenschaftlichen Daten zu rechnen.
54. ```python
    def arrow_head_matrix(n):
        matrix = np.eye(n)
        matrix[:,0] = np.ones(n)
        matrix[0,:] = np.ones(n)
        return matrix
    ```
55.  1.  Die Funktion muss ein Basis Status haben
     2.  Die Funktion muss ihren Status verändern und muss sich einem Basis Status nähern.
     3.  Die Funktion muss sich selber ausrufen.
56.  Von links nach rechts:
     1.   unterer Ausreißer
     2.   minimum
     3.   unteres Quartil
     4.   Mediam
     5.   oberes Quartil
     6.   maximum
     7.   oberer Ausreißer