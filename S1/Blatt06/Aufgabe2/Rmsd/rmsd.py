#!/usr/bin/env python3
#Bearbeitungszeit: 0.66 Stunden

import sys
from math import sqrt

def listofvectors_read(filename):
    try:
        stream = open(filename, 'r')
    except IOError as err:
        sys.stderr.write('{}: {}\n'.format(sys.argv[0], err))
        exit(1)

    vector_list = []

    for line in stream:
        try:
            vector_list.append(eval(line))
        except Exception as err:
            sys.stderr.write('{}: Failed to read "{}" because of {}\n'\
                    .format(sys.argv[0], line, err))
            exit(1)
    stream.close()
    return vector_list

def rmsd_evaluate(v, w):
    assert(len(v) == len(w))
    n = len(v)

    s = 0
    for i in range(n):
        assert(len(v[i]) == 3)
        assert(len(w[i]) == 3)
        s += (v[i][0] - w[i][0])**2 + \
                (v[i][1] - w[i][1])**2 + \
                (v[i][2] - w[i][2])**2

    return sqrt((1/n) * s)

def listofvectors_rmsd_print(listofvectors):
    for i in range(0, len(listofvectors) - 1, 2):
        rsmd = rmsd_evaluate(listofvectors[i], listofvectors[i + 1])
        print('rmsd(vector{},vector{})={:.5f}'.format(i + 1, i + 2, rsmd))

vectors = listofvectors_read('rmsd_input.csv')

listofvectors_rmsd_print(vectors)
