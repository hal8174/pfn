#!/usr/bin/env python3
# Bearbeitungszeit: 0.66 Stunden

import re, sys

def usage():
    sys.stderr.write("Usage: {} <linewidth> <inputfile>\n"
            .format(sys.argv[0]))
    exit(1)

if len(sys.argv) != 3:
    usage()

try:
    linewidth = int(sys.argv[1])
except ValueError:
    # Hier ist ein Fehler im Tamplate
    sys.stderr.write("{}: cannot convert {} into integer\n"
            .format(sys.argv[0], linewidth))
    usage()

filename = sys.argv[2]
try:
    stream = open(filename, "r")
except IOError as err:
    sys.stderr.write("{}: {}\n".format(filename, err))
    exit(1)

# add your code here
for line in stream:
    line = line.strip()
    while len(line) > linewidth:
        for i in range(linewidth, 0, -1):
            if line[i] == ' ':
                print(line[0:i])
                line = line[i+1:]
                break
    print(line)

stream.close()
