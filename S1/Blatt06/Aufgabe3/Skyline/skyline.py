#!/usr/bin/env python3
#Bearbeitungszeit: 0.33 Stunden

# len(s_i) = 2**i - 1

def skyline(i):
    if i == 1:
        return chr(97)
    prev = skyline(i - 1)
    return prev + chr(96 + i) + prev

for i in range(1, 10):
    print(skyline(i))
