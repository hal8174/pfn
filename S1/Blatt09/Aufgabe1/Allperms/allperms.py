#!/usr/bin/env python3

# Bearbeitungszeit: 1.5 Stunden

from math import factorial
import argparse


def all_permutations(elems):

    all_perms = [[]]

    all_perms_rec(all_perms, elems)

    return all_perms[:-1]


def all_perms_rec(all_perms, elems):
    if len(elems) == 0:

        all_perms.append([])
        return

    curr_perm = all_perms[-1].copy()

    for v in elems:

        new_elems = elems.copy()
        new_elems.remove(v)
        all_perms[-1] = curr_perm.copy()
        all_perms[-1].append(v)
        all_perms_rec(all_perms, new_elems)


def all_permutations_verify(all_perms, elems):

    assert(len(all_perms) == factorial(len(elems)))

    ges = set()

    for i in range(len(all_perms)):

        assert(tuple(all_perms[i]) not in ges)

        ges.add(tuple(all_perms[i]))

        assert(set(all_perms[i]) == set(elems))


if __name__ == '__main__':
    def parse_arguments():
        p = argparse.ArgumentParser(
                    description='generate and verify permutations')
        p.add_argument('max_setsize', type=int,
                    help='specify maximum size of set to permute')
        return p.parse_args()

    args = parse_arguments()
    for num_elems in range(0, args.max_setsize + 1):
        elems = list(range(num_elems))
        all_perms = all_permutations(elems)
        all_permutations_verify(all_perms, elems)
