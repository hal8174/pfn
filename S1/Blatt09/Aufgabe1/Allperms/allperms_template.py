#!/usr/bin/env python3

from math import factorial
import sys,argparse

def all_permutations(elems):
  # to be implemented
  return

def all_permutations_verify(all_perms,elems):
  # to be implemented
  return

if __name__ == '__main__':
  def parse_arguments():
    p = argparse.ArgumentParser(description='generate and verify permutations')
    p.add_argument('max_setsize',type=int,
                    help='specify maximum size of set to permute')
    return p.parse_args()

  args = parse_arguments()
  for num_elems in range(0,args.max_setsize + 1):
    elems = list(range(num_elems))
    all_perms = all_permutations(elems)
    all_permutations_verify(all_perms,elems)
