#!/usr/bin/env python3

# Bearbeitungszeit: 0.6 Stunden

import sys
import argparse


class Morse:
    morse_code = {
        'A': '.-',
        'B': '-...',
        'C': '-.-.',
        'D': '-..',
        'E': '.',
        'F': '..-.',
        'G': '--.',
        'H': '....',
        'I': '..',
        'J': '.---',
        'K': '-.-',
        'L': '.-..',
        'M': '--',
        'N': '-.',
        'O': '---',
        'P': '.--.',
        'Q': '--.-',
        'R': '.-.',
        'S': '...',
        'T': '-',
        'U': '..-',
        'V': '...-',
        'W': '.--',
        'X': '-..-',
        'Y': '-.--',
        'Z': '--..',
        '0': '-----',
        '1': '.----',
        '2': '..---',
        '3': '...--',
        '4': '....-',
        '5': '.....',
        '6': '-....',
        '7': '--...',
        '8': '---..',
        '9': '----.',
        '.': '.-.-.-',
        ',': '--..--',
        ' ': '------'
    }
    morse_code2_0 = {
        'A': '..--',
        'B': '-.-..--.',
        'C': '.....',
        'D': '.--.',
        'E': '--.',
        'F': '...-..',
        'G': '...-.-.',
        'H': '-.--.',
        'I': '....-',
        'J': '-.-..-.-',
        'K': '-.-..-..',
        'L': '-----',
        'M': '-.-.-',
        'N': '-..-',
        'O': '-...',
        'P': '-.-...--',
        'Q': '-.-...-.',
        'R': '---.',
        'S': '..-.',
        'T': '.---',
        'U': '...--.',
        'V': '-.-....-',
        'W': '-.-.....',
        'X': '...-.---',
        'Y': '...-.--.',
        'Z': '----.--',
        '0': '----.-..',
        '1': '-.------',
        '2': '-.-----.',
        '3': '-.----.-',
        '4': '-.----..',
        '5': '-.---.--',
        '6': '-.---.-.',
        '7': '-.---..-',
        '8': '-.---...',
        '9': '-.-..---',
        '.': '----..',
        ',': '----.-.-',
        ' ': '.-.'
    }

    def __init__(self, use2_0=False):
        self._use2_0 = use2_0
        self._morse_code_map = Morse.morse_code2_0 if use2_0 else Morse.morse_code

    def encode(self, text):

        res = ''

        for char in text:

            res += self._morse_code_map[char.upper()]

        return res

    def decode(self, morse_string):

        res = ''

        idx = 0

        while idx < len(morse_string):

            for char, morse in self._morse_code_map.items():

                if morse_string[idx:idx+len(morse)] == morse:

                    res += char
                    idx += len(morse)

        return res

# Im Morse Code werden Pausen vorausgesetzt. Dadurch sind die Kodierungen der
# einzelnen Buchstaben sehr kurz. Aber es gibt dann halt Buchstaben deren
# Kodierung auch in anderen Codierungen vorkommen.

# ... kann EEE oder S sein

# Wenn etwas Decodiert, das Decodierte wieder Encodiert wird und das Ergebnis
# gleich dem Anfang ist, heißt das, dass zumindest die Kodierung in sich
# Funktioniert. Also können damit grobe Fehler in der Kodierung ausgeschlossen
# werden.

# wave files are from
# https://drive.google.com/drive/folders/0B9hr-DdPL7utdkFLYXVXN25nZXc


def play_morse(morse_string):
    print('#!/bin/sh')
    print('OS=`uname -s`')
    print('if test ${OS} == "Darwin"')
    print('then')
    print('    SOUNDPLAYER=afplay')
    print('else')
    print('    if test ${OS} == "Linux"')
    print('    then')
    print('        SOUNDPLAYER=aplay')
    print('    else')
    print('        echo "$0: unknown operating system ${OS}"')
    print('        exit 1')
    print('    fi')
    print('fi')
    for m in morse_string:
        assert m == '.' or m == '-'
        print('${{SOUNDPLAYER}} {}.wav'.format('dah' if m == '.' else 'dit'))


def parse_arguments():
    p = argparse.ArgumentParser(description=('encoding text into   morse code '
                'and vice versa'))
    p.add_argument('--mc2_0', action='store_true', default=False,
                help='use morse code in version 2.0')
    outputgroup = p.add_mutually_exclusive_group(required=False)
    outputgroup.add_argument('-t', '--text', action='store_true', default=False,
                help='output morse code as text')
    outputgroup.add_argument('-d', '--decode', action='store_true', default=False,
                help='decode given morse string, implies option --mc2_0')
    p.add_argument('string', type=str, metavar='string or filename',
                help='specify input string or file name, - for stdin')
    args = p.parse_args()
    if args.decode:
        args.mc2_0 = True
    return args


args = parse_arguments()

mc = Morse(args.mc2_0)

inputstring = None
file_input = True
if args.string == '-':
    stream = sys.stdin
    file_input = False
else:
    try:
        stream = open(args.string)
    except IOError:
        inputstring = args.string
        file_input = False

if not inputstring:
    assert stream
    inputstring = stream.read().rstrip()

if file_input:
    stream.close()

if args.decode:
    text = mc.decode(inputstring)
    print(text)
else:
    morse_string = mc.encode(inputstring)
    if args.text:
        print(morse_string)
    else:
        play_morse(morse_string)
