#!/usr/bin/env python3
#Bearbeitungszeit: 0.33 Stunden

import sys

if len(sys.argv) != 3:
    sys.stderr.write('Usage: {} <filename-template> <filename-tsv>'
            .format(sys.argv[0]))
    exit(1)

try:
    stream = open(sys.argv[2], 'r')
except IOError as err:
    sys.stderr.write('{}: {}\n'.format(sys.argv[0], err))
    exit(1)

values = dict()
for line in stream:
    temp = line.strip().split('\t')
    values[temp[0]] = temp[1]

stream.close()

try:
    stream = open(sys.argv[1], 'r')
except IOError as err:
    sys.stderr.write('{}: {}\n'.format(sys.argv[0], err))
    exit(1)

for line in stream:
    for key, value in values.items():
        line = line.replace(key, value)
    print(line, end='')
