#!/usr/bin/env python3
#Bearbeitungszeit: 0.5 Stunden

import math

x = [6,7,12,14,23,41,53,60,69,72,100,90]
y = [2.5,1.1,6.3,2.1,2.9,15.3,20.7,18.4,22,33,50,43]
z = [1,12,18,33,78,99,65,77,81,54,78,77]

def compare(a, b):
    assert len(a) == len(b) and len(a) > 0
    a_m = sum(a) / len(a)
    b_m = sum(b) / len(b)
    a_b_diff = a_diff = b_diff = 0
    for i in range(len(a)):
        a_b_diff += (a[i] - a_m) * (b[i]-b_m)
        a_diff += (a[i] - a_m)**2
        b_diff += (b[i] - b_m)**2
    return a_b_diff / math.sqrt(a_diff * b_diff)

print('x_vector/y_vector: {:.5f}'.format(compare(x, y)))
print('x_vector/z_vector: {:.5f}'.format(compare(x, z)))
print('y_vector/z_vector: {:.5f}'.format(compare(y, z)))
