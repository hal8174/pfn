#!/usr/bin/env python3
# Bearbeitungszeit: 0.25 Stunden

import argparse

#Hier die Deklaration von cigarXevalargparse() einfuegen.
def cigarXevalargparse():
    p = argparse.ArgumentParser()
    p.add_argument('-c', '--cost', action='store_true', default=False,
            help='show cost for each cigarXstring')
    p.add_argument('-i', '--identity', action='store_true', default=False,
            help='show identity for each cigarXstring')
    p.add_argument('inputfile', type=str,
            help='specify input file')
    return p.parse_args()


if __name__ == "__main__":
    cigarXevalargparse()
