#!/usr/bin/env python3

# Bearbeitungszeit: 0.25 Stunden

print('# celsius\tfahrenheit')

for i in range(1, 101):

    print('{:.2f}\t{:.2f}'.format(i, (i * 9) / 5 + 32))
