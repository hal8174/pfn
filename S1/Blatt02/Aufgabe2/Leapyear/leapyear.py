#!/usr/bin/env python3

# Bearbeitungszeit: 0.25 Stunden

import sys

for year_string in sys.argv[1:]:

    year = int(year_string)

    if year % 400 == 0 or (year % 4 == 0 and year % 100 != 0):

        print('{} ist ein Schaltjahr'.format(year_string))

    else:

        print('{} ist kein Schaltjahr'.format(year_string))
