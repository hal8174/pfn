#!/bin/sh

tail -n +2 islands.tsv > nohead.tsv

grep "hypothetical protein" nohead.tsv > hypo.tsv

grep -v "hypothetical protein" nohead.tsv > nothypo.tsv

cut -f 7 nohead.tsv > locus.tsv

cut -f 1,2 nohead.tsv | sort -un > ranges.tsv
