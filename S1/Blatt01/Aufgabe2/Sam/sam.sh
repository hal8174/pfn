#!/bin/sh

# Bearbeitungszeit: 3.5 Stunden

grep -v '^@' alignments.sam | grep -v "*" | cut -f 3 | sort -u > references.txt

grep -v '^@' alignments.sam | grep -f references.txt > aligned_only.sam

grep -v '^@' alignments.sam | cut -f 3 | sort -d | uniq -c | tr -s ' ' | tr ' ' '\t' | cut --fields=2- > references_count.tsv

grep -v '^@' alignments.sam | grep -f references.txt | cut -f 1 | sort -d | uniq -c | tr -s ' ' | tr ' ' '\t' | cut --fields=2- > reads_n_alignments.tsv

cut -f 1 reads_n_alignments.tsv | sort -n | uniq -c | tr -s ' ' | tr ' ' '\t' | cut --fields=2- > reads_n_alignments_distri.tsv



