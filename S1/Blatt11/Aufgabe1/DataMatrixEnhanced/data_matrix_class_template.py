#!/usr/bin/env python3

import re, sys

class DataMatrix:
  def __init__(self,lines,key_col = 1,sep = '\t'):
    self._matrix = dict()
    self._attribute_list = None
    for line in lines:
      ls = line.rstrip('\n').split(sep)
      if self._attribute_list is None: # in first line
        self._attribute_list = ls
      else:  # not in first line: values
        if len(ls) != len(self._attribute_list):
          sys.stderr.write('line has {} columns, but {} expected\n'
                            .format(len(ls),len(self._attribute_list)))
          exit(1)
        line_dict = dict()
        for attr, value in zip(self._attribute_list,ls):
          line_dict[attr] = value
        k = ls[key_col]
        if k in self._matrix:
          sys.stderr.write('key {} in line {} is not unique\n'
                          .format(k,2+len(self._matrix)))
          exit(1)
        self._matrix[k] = line_dict
  def keys(self):
    return self._matrix.keys()
  def attribute_list(self):
    return self._attribute_list
  def show(self,sep,attributes,keys):
    for key in keys:
      for a in attributes:
        if self._matrix[key][a] != '':
          print('{}{}{}{}{}'
                 .format(key,sep,a,sep,self._matrix[key][a]))
  def show_orig(self,sep,attributes,keys):
    print(sep.join(attributes))
    for key in keys:
      line_elems = list()
      if not key in self._matrix:
        sys.stderr.write('{}: illegal key "{}"\n'.format(sys.argv[0],key))
        exit(1)
      for a in attributes:
        if not a in self._matrix[key]:
          sys.stderr.write('{}: illegal attribute "{}"\n'.format(sys.argv[0],a))
          exit(1)
        line_elems.append(self._matrix[key][a])
      print(sep.join(line_elems))
 
import sys, argparse

def parse_command_line():
  p = argparse.ArgumentParser(description='handle data matrices')
  p.add_argument('-k','--keys',nargs='+',default=None,metavar='<key>',
                  help='specify keys for which values are output')
  p.add_argument('-a','--attributes',nargs='+',default=None,
                 metavar='<attribute>',
                 help='specify attributes output')
  p.add_argument('-o','--orig',action='store_true',default=False,
                  help='output key/value pairs in original format')
  p.add_argument('-s','--sep',type=str,default='\t',
                  help='specify column separator, default is Tab')
  p.add_argument('inputfile',type=str,
                  help='specify inputfile (mandatory argument)')
  return p.parse_args()

def main():
  args = parse_command_line()
  try:
    stream = open(args.inputfile)
  except IOError as err:
    sys.stderr.write('{}: cannot open file {}\n'
                      .format(sys.argv[0],args.inputfile))
    exit(1)
  data_matrix = DataMatrix(stream,1,args.sep)
  stream.close()
  if args.attributes:  # option -a specifies attributes
    attributes = args.attributes
  else:
    attributes = data_matrix.attribute_list() # use all attributes
  if args.keys:
    keys = args.keys # use -k specified keys
  else:
    keys = data_matrix.keys() # use all keys
  if args.orig:
    data_matrix.show_orig(args.sep,attributes,keys)
  else:
    data_matrix.show(args.sep,attributes,keys)

if __name__ == '__main__':
  main()
