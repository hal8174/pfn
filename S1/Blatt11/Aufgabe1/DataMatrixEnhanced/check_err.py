#!/usr/bin/env python3

from mysubprocess import mysubprocess_expect

mysubprocess_expect('./data_matrix_class.py --key_col 20 atom-data.tsv ',
                    1,'./data_matrix_class.py: number of column for keys '
                      'is out of range')

mysubprocess_expect('./data_matrix_class.py -m -o atom-data.tsv',
                    1, './data_matrix_class.py: option -o/--orig is '
                       'incompatible with option -m/--mean')

mysubprocess_expect('./data_matrix_class.py -m --sort_ascending atomicMass atom-data.tsv',
                    1, './data_matrix_class.py: option --sort_ascending is '
                       'incompatible with option -m/--mean')
