#!/usr/bin/env python3

# Bearbeitungszeit: 2 Stunden

import argparse
import re
import sys
from operator import itemgetter


class DataMatrix:
    def __init__(self, lines, key_col=1, sep='\t'):
        self._matrix = dict()
        self._attribute_list = None
        for line in lines:
            ls = line.rstrip('\n').split(sep)
            if self._attribute_list is None:  # in first line
                self._attribute_list = ls
                if key_col < 0 or key_col >= len(ls):
                    sys.stderr.write(
                        '{}: number of column for keys is out of range\n'
                        .format(sys.argv[0]))
                    exit(1)
            else:  # not in first line: values
                if len(ls) != len(self._attribute_list):
                    sys.stderr.write('line has {} columns, but {} expected\n'
                                     .format(len(ls),
                                             len(self._attribute_list)))
                    exit(1)
                line_dict = dict()
                for attr, value in zip(self._attribute_list, ls):
                    line_dict[attr] = value
                k = ls[key_col]
                if k in self._matrix:
                    sys.stderr.write('key {} in line {} is not unique\n'
                                     .format(k, 2+len(self._matrix)))
                    exit(1)
                self._matrix[k] = line_dict

    def keys(self):
        return self._matrix.keys()

    def attribute_list(self):
        return self._attribute_list

    def show(self, sep, attributes, keys):
        for key in keys:
            for a in attributes:
                if self._matrix[key][a] != '':
                    print('{}{}{}{}{}'
                          .format(key, sep, a, sep, self._matrix[key][a]))

    def show_orig(self, sep, attributes, keys):
        print(sep.join(attributes))
        for key in keys:
            line_elems = list()
            if not key in self._matrix:
                sys.stderr.write(
                    '{}: illegal key "{}"\n'.format(sys.argv[0], key))
                exit(1)
            for a in attributes:
                if not a in self._matrix[key]:
                    sys.stderr.write(
                        '{}: illegal attribute "{}"\n'.format(sys.argv[0], a))
                    exit(1)
                line_elems.append(self._matrix[key][a])
            print(sep.join(line_elems))

    def show_attribute_mean(self, attributes, keys):
        for a in attributes:
            s = 0
            c = 0
            for key in keys:
                #print(a, key, self._matrix[key][a])
                if re.match(r'^-?\d+\.?\d*$', self._matrix[key][a]):
                    s += float(self._matrix[key][a])
                    c += 1
                elif self._matrix[key][a] != '':
                    s = None
                    break
            if not s is None:
                print('Mean {}: {:.2f}'.format(a, s/c))

    def show_ascending(self, attribute):
        keys = self.sort_keys(attribute)
        for key in keys:
            print('{}\t{}\t{}'.format(
                key, attribute, self._matrix[key][attribute]))

    def sort_keys(self, attribute):
        valuekey_list = []
        for key in self._matrix.keys():
            valuekey_list.append((self._matrix[key][attribute], key))
        valuekey_list.sort()
        return [x[1] for x in valuekey_list]


def parse_command_line():
    p = argparse.ArgumentParser(description='handle data matrices')
    p.add_argument('-k', '--keys', nargs='+', default=None, metavar='<key>',
                   help='specify keys for which values are output')
    p.add_argument('--key_col', type=int, default=1,
                   help='specefy colomn to be used for keys')
    p.add_argument('-m', '--mean', default=False, action='store_true',
                   help='calculate means')
    p.add_argument('--sort_ascending', type=str,
                   help='sort ascending')
    p.add_argument('-a', '--attributes', nargs='+', default=None,
                   metavar='<attribute>',
                   help='specify attributes output')
    p.add_argument('-o', '--orig', action='store_true', default=False,
                   help='output key/value pairs in original format')
    p.add_argument('-s', '--sep', type=str, default='\t',
                   help='specify column separator, default is Tab')
    p.add_argument('inputfile', type=str,
                   help='specify inputfile (mandatory argument)')
    return p.parse_args()


def main():
    args = parse_command_line()
    try:
        stream = open(args.inputfile)
    except IOError:
        sys.stderr.write('{}: cannot open file {}\n'
                         .format(sys.argv[0], args.inputfile))
        exit(1)
    data_matrix = DataMatrix(stream, args.key_col, args.sep)
    stream.close()
    if args.attributes:  # option -a specifies attributes
        attributes = args.attributes
    else:
        attributes = data_matrix.attribute_list()  # use all attributes
    if args.keys:
        keys = args.keys  # use -k specified keys
    else:
        keys = data_matrix.keys()  # use all keys
    if args.mean:
        if args.orig:
            sys.stderr.write(
                '{}: option -o/--orig is incompatible with option -m/--mean\n'.format(sys.argv[0]))
            exit(1)
        if args.sort_ascending:
            sys.stderr.write(
                '{}: option --sort_ascending is incompatible with option -m/--mean\n'.format(sys.argv[0]))
            exit(1)
        data_matrix.show_attribute_mean(attributes, keys)
    elif args.sort_ascending:
        data_matrix.show_ascending(args.sort_ascending)
    elif args.orig:
        data_matrix.show_orig(args.sep, attributes, keys)
    else:
        data_matrix.show(args.sep, attributes, keys)


if __name__ == '__main__':
    main()
