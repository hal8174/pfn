import sys

# lst{LennjoImport}
import math
from lennjo_data import get_epsilon, get_sigma
#lstend#


class Atom:
    def __init__(self, ident, name, x, y, z, atomtype, optional_list):
        self._ident = ident
        self._name = name
        self._x = float(x)
        self._y = float(y)
        self._z = float(z)
        self._atomtype = atomtype
        self._optional_list = optional_list

    def name(self):
        return self._name

    def __str__(self):
        outlist = [self._ident, self._name]
        for f in [self._x, self._y, self._z]:
            outlist.append('{:.4f}'.format(f))
        outlist.append(self._atomtype)
        if self._optional_list:
            outlist += self._optional_list
        return ' '.join(outlist)
# lst{LennjoAtomSubMethod}

    def __sub__(self, other):
        dist_x = self._x - other._x
        dist_y = self._y - other._y
        dist_z = self._z - other._z
        return math.sqrt((dist_x * dist_x) + (dist_y * dist_y) +
                         (dist_z * dist_z))
#lstend#


class Bond:
    def __init__(self, ident, atomid1, atomid2, kind, optional_list):
        self._ident = ident
        self._atomid1 = atomid1
        self._atomid2 = atomid2
        self._kind = kind
        self._optional_list = optional_list

    def atomid1(self):
        return self._atomid1

    def atomid2(self):
        return self._atomid2

    def __str__(self):
        outlist = [self._ident, self._atomid1, self._atomid2, self._kind]
        if self._optional_list:
            outlist += self._optional_list
        return ' '.join(outlist)


class Molecule:
    def __init__(self, molecule_name, atom_list, bond_list):
        self._molecule_name = molecule_name
        self._atoms = list()
        self._bonds = list()
        for a in atom_list:
            self._atoms.append(Atom(a[0], a[1], a[2], a[3], a[4], a[5], a[6:]))
        for b in bond_list:
            self._bonds.append(Bond(b[0], b[1], b[2], b[3], b[4:]))

    def name(self):
        return self._molecule_name

    def atoms_number(self):
        return len(self._atoms)

    def atom_list(self):
        return self._atoms

    def bonds_number(self):
        return len(self._bonds)

    def __str__(self):
        lines = list()
        lines.append('@<TRIPOS>MOLECULE')
        lines.append(self._molecule_name)
        lines.append('@<TRIPOS>ATOM')
        for a in self._atoms:
            lines.append(str(a))
        lines.append('@<TRIPOS>BOND')
        for b in self._bonds:
            lines.append(str(b))
        return '\n'.join(lines)
# lst{LennjoMoleculeSubMethod}

    def __sub__(self, other):  # should be modified to compute LJ-Potential
        e_pair = 0.0
        for a in self.atom_list():
            epsilon_a = get_epsilon(a._name)
            sigma_a = get_sigma(a._name)
            for c in other.atom_list():
                epsilon_c = get_epsilon(c.name())
                sigma_c = get_sigma(c.name())
                dist_R = a - c
                epsilon_ab = math.sqrt(epsilon_a * epsilon_c)
                sigma_ab = 0.5 * (sigma_a + sigma_c)
                e6 = math.pow(sigma_ab/dist_R, 6)
                e12 = e6 * e6
                e_pair += 4.0 * epsilon_ab * (e12 - e6)
        return e_pair
#lstend#

    def adjacency_matrix(self):  # needed for shortest path algorithm
        d = []
        for i in range(self.atoms_number()):
            d.append([])
            for j in range(self.atoms_number()):
                neighbours = False
                for bond in self._bonds:
                    if (int(bond.atomid1()) == i + 1 and
                        int(bond.atomid2()) == j + 1) or\
                        (int(bond.atomid1()) == j + 1 and
                         int(bond.atomid2()) == i + 1):
                        neighbours = True
                        break
                if neighbours:
                    d[i].append(1)
                else:
                    d[i].append(0)
        return d
