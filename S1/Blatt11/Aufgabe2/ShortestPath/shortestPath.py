#!/usr/bin/env python3

import argparse
from molecule import Molecule
from mol2iter import mol2Iterator
from floyd_warshall import floyd_warshall

def show_pathlength_matrix(molecule,dmatrix):
  rows = len(dmatrix)
  atom_list = molecule.atom_list()
  line_list = [''] + [atom_list[i].name() for i in range(rows)]
  print('\t'.join(line_list))
  for i in range(rows):
    line_list = [atom_list[i].name()]
    for j in range(rows):
      line_list.append(str(dmatrix[i][j]))
    print('\t'.join(line_list))

def show_path_of_length(dmatrix,molecule,length):
  rows = len(dmatrix)
  atom_list = molecule.atom_list()
  for i in range(rows):
    for j in range(i+1,rows):
      if dmatrix[i][j] == length:
        print('path\t{}\t{}\t{}\t{}'
               .format(i+1,j+1,atom_list[i].name(),
                               atom_list[j].name()))

def parse_arguments():
  p = argparse.ArgumentParser(description=('Compute length of shortest paths '
                                           'in molecule-graphs for molecules '
                                           'stored in mol2 file'))
  p.add_argument('-s','--show_matrix',action='store_true',default=False,
                 help='show matrix of shortest paths length')
  p.add_argument('-l','--length',metavar='<pathlength>',type=int,default=3,
                 help='specify length of path (default: 3)')
  p.add_argument('inputfile',type=str,metavar='filename',
                  help='specify input file in mol2-format')
  return p.parse_args()

args = parse_arguments()

for molecule_name, atom_list, bond_list in mol2Iterator(args.inputfile):
  molecule = Molecule(molecule_name,atom_list,bond_list)
  amatrix = molecule.adjacency_matrix()
  dmatrix = floyd_warshall(amatrix)
  rows = len(dmatrix)
  print('molecule\t{}'.format(molecule_name))
  show_path_of_length(dmatrix,molecule,args.length)
  if args.show_matrix:
    show_pathlength_matrix(molecule,dmatrix)
