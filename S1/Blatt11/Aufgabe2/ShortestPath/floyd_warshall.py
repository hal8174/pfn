
# Bearbeitungszeit: 0.8 Stunden


def floyd_warshall(d):

    for i in range(len(d)):
        for j in range(len(d)):
            if d[i][j] == 0:
                d[i][j] = float('inf')

    for k in range(len(d)):
        for i in range(len(d)):
            for j in range(len(d)):
                if i != j:
                    d[i][j] = min(d[i][j], d[i][k] + d[k][j])

    return d
