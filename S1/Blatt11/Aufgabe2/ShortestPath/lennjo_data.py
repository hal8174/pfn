# (Einheiten: epsilon in kcal/mol, sigma in AA)
# Daten stammen aus OPLS-AA-Kraftfeld, vereinfachte Annahme: nur 1 Satz
# Parameter pro Atomsorte

import sys, re

def get_epsilon(elem):
  lj_eps = { # class variable
    'H' :0.1260,
    'He':0.0200,
    'C' :0.1050,
    'N' :0.1700,
    'O' :0.2100,
    'Ne':0.0690,
    'Cl':0.3000,
    'Ar':0.2339,
    }
  elem = re.sub(r'[0-9]$','',elem)
  if elem in lj_eps:
    return lj_eps[elem]
  else:
    sys.stderr.write('{}: no eps-value for {}\n'
                      .format(sys.argv[0],elem))
    exit(1)

def get_sigma(elem):
  lj_sig = { # class variable
    'H' :2.420,
    'He':2.556,
    'C' :3.750,
    'N' :3.200,
    'O' :2.960,
    'Ne':2.780,
    'Cl':3.400,
    'Ar':3.401,
    }
  elem = re.sub(r'[0-9]$','',elem)
  if elem in lj_sig:
    return lj_sig[elem]
  else:
    sys.stderr.write('{}: no sig-value for {}\n'
                     .format(sys.argv[0],elem))
    exit(1)
