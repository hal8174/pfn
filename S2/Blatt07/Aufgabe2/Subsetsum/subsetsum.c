#include "subsetsum.h"
#include <stddef.h>
#include <stdlib.h>
#include "array2dim.h"
#include <stdio.h>
/*
bool subsetsum_hs(const unsigned long *arr,
                  unsigned long r,
                  unsigned long s)
{
    if (r == 0)
    {
        return true;
    }
    if (s == 0)
    {
        return false;
    }
    if (s >= arr[r - 1] && subsetsum_hs(arr, r - 1, t - arr[r - 1]) == true)
    {
        return true;
    }
    else
    {
        return subsetsum_hs(arr, r - 1, t);
    }
}
*/
bool subsetsum_calulate(const unsigned long *arr, bool *mark,
                        unsigned long r,
                        unsigned long s)
{
    if (s == 0)
    {
        return true;
    }
    if (r == 0)
    {
        return false;
    }
    if (s >= arr[r - 1] && subsetsum_calulate(arr, mark, r - 1, s - arr[r - 1]))
    {
        mark[r - 1] = true;
        return true;
    }
    mark[r - 1] = false;
    return subsetsum_calulate(arr, mark, r - 1, s);
}

bool *subsetsum(const unsigned long *arr,
                unsigned long r,
                unsigned long s)
{
    unsigned long i;
    bool *mark;
    bool result;
    mark = malloc(r * sizeof mark);
    for (i = 0; i < r; i++)
    {
        mark[i] = false;
    }

    result = subsetsum_calulate(arr, mark, r, s);

    if (result)
    {
        return mark;
    }
    else
    {
        free(mark);
        return NULL;
    }
}

typedef struct
{
    bool defined, has_sol;
} DefinedValue;

bool subsetsum_memo_calulate(DefinedValue **cache, const unsigned long *arr, bool *mark,
                             unsigned long r,
                             unsigned long s)
{
    if (cache[r][s].defined)
    {
        return cache[r][s].has_sol;
    }
    if (s == 0)
    {
        return true;
    }
    if (r == 0)
    {
        return false;
    }
    bool result;
    if (s >= arr[r - 1])
    {
        result = subsetsum_memo_calulate(cache, arr, mark, r - 1, s - arr[r - 1]);
        cache[r][s].defined = true;
        cache[r][s].has_sol = result;
        if (result)
        {
            mark[r - 1] = true;
            return true;
        }
    }
    mark[r - 1] = false;
    result = subsetsum_memo_calulate(cache, arr, mark, r - 1, s);
    cache[r][s].defined = true;
    cache[r][s].has_sol = result;
    return result;
}

bool *subsetsum_memo(const unsigned long *arr,
                     unsigned long r,
                     unsigned long s)
{
    unsigned long i;
    unsigned long j;

    bool *mark;
    bool result;
    mark = malloc(r * sizeof mark);
    for (i = 0; i < r; i++)
    {
        mark[i] = false;
    }
    DefinedValue **cache;
    array2dim_malloc(cache, DefinedValue, r + 1, s + 1);

    result = subsetsum_memo_calulate(cache, arr, mark, r, s);

    for (i = 0; i <= r; i++)
    {
        for (j = 0; j <= s; j++)
        {
            cache[i][j].defined = false;
        }
    }

    array2dim_delete(cache);

    if (result)
    {
        return mark;
    }
    else
    {
        free(mark);
        return NULL;
    }
}