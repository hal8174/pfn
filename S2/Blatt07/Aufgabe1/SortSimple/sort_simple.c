#include "sort_simple.h"
#include <stdio.h>
#include <string.h>

int lex_sort(const void *a, const void *b)
{
    return strcmp(*(const char **)a, *(const char **)b);
}

int rev_lex_sort(const void *a, const void *b)
{
    return -1 * lex_sort(a, b);
}

int num_sort(const void *a, const void *b)
{
    double a_double;
    double b_double;
    bool a_number = true;
    bool b_number = true;

    if (sscanf(*(const char **)a, "%lf", &a_double) != 1)
    {
        if (strlen(*(const char **)a) == 0)
        {
            a_double = 0.0;
        }
        else
        {
            a_number = false;
        }
    }
    if (sscanf(*(const char **)b, "%lf", &b_double) != 1)
    {
        if (strlen(*(const char **)b) == 0)
        {
            b_double = 0.0;
        }
        else
        {
            b_number = false;
        }
    }

    if (a_number == false && b_number == false)
    {
        return lex_sort(a, b);
    }
    if (a_number == false)
    {
        return -1;
    }
    if (b_number == false)
    {
        return 1;
    }

    if (a_double > b_double)
    {
        return 1;
    }
    else if (a_double < b_double)
    {
        return -1;
    }
    return 0;
}

int rev_num_sort(const void *a, const void *b)
{
    return -1 * num_sort(a, b);
}

void sort_simple(PfNLineStore *line_store, bool numerical_order,
                 bool reverse_order)
{
    if (numerical_order)
    {
        if (reverse_order)
        {
            pfn_line_store_sort(line_store, rev_num_sort);
        }
        else
        {
            pfn_line_store_sort(line_store, num_sort);
        }
    }
    else
    {
        if (reverse_order)
        {
            pfn_line_store_sort(line_store, rev_lex_sort);
        }
        else
        {
            pfn_line_store_sort(line_store, lex_sort);
        }
    }
}
