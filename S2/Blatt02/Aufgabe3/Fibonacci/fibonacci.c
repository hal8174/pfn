#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#ifdef SMALLVALUES
typedef unsigned short FibonacciNumberType;
#define FibonacciNumberType_max USHRT_MAX
#define FibonacciNumberType_print(I,F) printf("%d\t%hu\n",I,F)
#else
#ifdef MEDIUMVALUES
typedef unsigned int FibonacciNumberType;
#define FibonacciNumberType_max UINT_MAX
#define FibonacciNumberType_print(I,F) printf("%d\t%u\n",I,F)
#else
typedef unsigned long FibonacciNumberType;
#define FibonacciNumberType_max ULONG_MAX
#define FibonacciNumberType_print(I,F) printf("%d\t%lu\n",I,F)
#endif
#endif

int main(void)
{
  FibonacciNumberType t; // temp variable
  FibonacciNumberType p = 0; // previous element
  FibonacciNumberType c = 1; // current element

  FibonacciNumberType_print(0, p);
  FibonacciNumberType_print(1, c);

  int i = 2;
  while (c < FibonacciNumberType_max / 2)
  {
    t = c + p;
    p = c;
    c = t;
    FibonacciNumberType_print(i, c);
    i++;
  }

  return EXIT_SUCCESS;
}
