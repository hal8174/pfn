#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

int main ()
{
    int n;
    char dict[] = {'a', 'b', 't', 'n', 'v', 'f', 'r'};
    for (char r = 0; r < 32; r++)
    {
        for (char c = 0; c < 4; c++)
        {
            n = r + c * 32;
            if (n >= 33 && n <= 126)
            {
                printf("%3d%5c", n, n);
            }
            else if (n > 6 && n < 14)
            {
                printf("%3d   \\%c", n, dict[n-7]);
            }
            else
            {
                printf("%3d%s\\%d", n, n < 10 ? "   " : (n < 100 ? "  ": " "), n);
            }
            if (c < 3)
            {
                printf("\t");
            }
        }
        printf("\n");
    }
    

    return EXIT_SUCCESS;
}