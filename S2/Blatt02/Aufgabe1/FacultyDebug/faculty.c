#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
  int n;
  if (argc != 2 || sscanf(argv[1],"%d",&n) != 1 || n < 0)
  {
    fprintf(stderr, "Usage: {} <positive_integer %s>\t\n", argv[1]);
    return EXIT_FAILURE;
  }

  unsigned long value = 1;
  for (int count = 1; count <= n; count++)
  {
    value *= (unsigned long) count;
  }
  printf("%d\t%lu\n", n,value);
  return EXIT_SUCCESS;
}
