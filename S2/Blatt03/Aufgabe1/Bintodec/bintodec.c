#include <limits.h>

#include "bintodec.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define NUMOFBITS 32/* number of bits in bitvector */
#define BITVECTOR_MAX_WIDTH (NUMOFBITS + NUMOFBITS/CHAR_BIT - 1)

char *decimal2bitvector(unsigned int decimal)
{
    char *bitvector;

    bitvector = (char*) malloc((BITVECTOR_MAX_WIDTH+1) * sizeof(*bitvector));

    if (bitvector == NULL) {
        fprintf(stderr, "malloc(%lu)  failed\n", (BITVECTOR_MAX_WIDTH+1) * sizeof(*bitvector));
        exit(EXIT_FAILURE);
    }

    int i, power;
    for (i = 0, power = NUMOFBITS - 1; i <= BITVECTOR_MAX_WIDTH; i++)
    {
        //printf("Bitvector: %s; i: %d; power: %u; decimal: %u\n", bitvector, i, ((unsigned int) 1) << power, decimal);
        if (i % 9 == 8)
        {
            bitvector[i] = ' ';
        }
        else
        {
            if (decimal >= ((unsigned int) 1) << power)
            {
                decimal -= ((unsigned int) 1) << power;
                bitvector[i] = '1';
            } 
            else
            {
                bitvector[i] = '0';
            }
            power--;
        }
    }
    
    bitvector[BITVECTOR_MAX_WIDTH] = 0x00;
    //printf("Bitvector: %s\n", bitvector);

    return bitvector;
}

int bitvector_validate(const char *bitvector)
{
    if (strlen(bitvector) < NUMOFBITS || strlen(bitvector) > BITVECTOR_MAX_WIDTH)
    {
        fprintf(stderr, "bitvector_validate: Bitvector length out of bounds\n");
        return 1;
    }

    int i;
    int numbers = 0;
    for (i = 0; i < strlen(bitvector); i++)
    {
        if (bitvector[i] == '0' || bitvector[i] == '1')
        {
            numbers ++;
        } 
        else if (bitvector[i] != ' ')
        {
            fprintf(stderr, "bitvector_validate: Invalid characters in string\n");
            return 1;
        }

    }
    
    if (numbers != NUMOFBITS) {
        fprintf(stderr, "bitvector_validate: Number of bits not correct\n");
        return 1;
    }


    return 0;
}

unsigned int bitvector2decimal(const char *bitvector)
{
    //printf("Bitvector: %s\n", bitvector);
    int index = strlen(bitvector);
    int power = 0;
    unsigned int result = 0;
    while (index >= 0)
    {
        //printf("Index: %d; Power: %d; result: %u\n", index, power, result);
        if (bitvector[index] == '1')
        {
            result += ((unsigned int) 1) << power;
            power++;
        }
        else if (bitvector[index] == '0')
        {
            power++;
        }
        index--;
    }
    return result;
}