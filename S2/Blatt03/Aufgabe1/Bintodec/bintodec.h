#ifndef BINTODEC_H
#define BINTODEC_H
char *decimal2bitvector(unsigned int decimal);
int bitvector_validate(const char *bitvector);
unsigned int bitvector2decimal(const char *bitvector);
#endif
