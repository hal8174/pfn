
data <- read.table("sinexp.dat" ,sep=" ", header=TRUE)

sinexp <- function (x, phase, freq, decay) {
    return (sin(x * freq + phase) * exp(-1 * decay * x))
}

nlmod <- nls(y~sinexp(x, phase, freq, decay), data=data, start=list(phase=6, freq=100*2*pi, decay=30))

png(file='fitting.png')

plot (data$x, data$y, xlab = expression (italic(x)), ylab="amplitude")
lines(data$x, predict(nlmod), col = 3, lwd = 3)

dev.off()