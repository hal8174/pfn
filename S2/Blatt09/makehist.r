

data <- read.table("uebung_simple/blast_clupea_harengus.out", skip=3 ,sep=" ", header=TRUE)

xlab_text = expression(paste("log"["10"], italic(' e'), '-value'))

nr <- nrow(data)
myname <- "henning"
info = paste(nr, " sequences\n", myname)

png(file="histogram.png")

hist(log10(data$exp_val), xlab=xlab_text, main=info)

dev.off()