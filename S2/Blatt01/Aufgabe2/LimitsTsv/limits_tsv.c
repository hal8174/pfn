#include <limits.h>
#include <stdio.h>

#define SPRINT(TYPE, MIN, MAX)\
    (printf("%s\t%ld\t%ld\t%ld\t%.2e\n", (#TYPE), CHAR_BIT * sizeof(TYPE),\
    (long)(MIN), (long)(MAX), (double)(MAX)))

#define UPRINT(TYPE, MAX)\
    (printf("%s\t%lu\t%lu\t%lu\t%.2e\n", (#TYPE), CHAR_BIT * sizeof(TYPE),\
    0UL, (unsigned long)(MAX), (double)(MAX)))

int main (void) {

    printf("# type	bits	min	max	max\n");
    
    SPRINT(char, CHAR_MIN, CHAR_MAX);
    SPRINT(short, SHRT_MIN, SHRT_MAX);
    SPRINT(int, INT_MIN, INT_MAX);
    SPRINT(long, LONG_MIN, LONG_MAX);

    UPRINT(unsigned char, UCHAR_MAX);
    UPRINT(unsigned short, USHRT_MAX);
    UPRINT(unsigned int, UINT_MAX);
    UPRINT(unsigned long, ULONG_MAX);

    return 0;

}