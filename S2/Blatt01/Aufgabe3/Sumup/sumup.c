#include <stdio.h>
#include <assert.h>

int main (int argc, char *argv[]) {

    if (argc != 3) {
        printf("%s: Wrong number of arguments.\n", argv[0]);
        return 1;
    }

    unsigned long m;
    unsigned long n;

    if (sscanf(argv[1], "%lu", &m) != 1) {
        printf("%s: m is invalid\n", argv[0]);
        return 1;
    }

    if (sscanf(argv[2], "%lu", &n) != 1) {
        printf("%s: n is invalid\n", argv[0]);
        return 1;
    }

    unsigned long sum1 = 0;
    unsigned long sum2 = 0;

    if (m <= n){

        for (unsigned long i = m; i <= n; i++)
        {
            sum1 += i;
        }
        
        sum2 = (n * (n+1) - (m-1) * m) / 2;

        assert(sum1 == sum2);
    
    }

    printf("%lu\t%lu\t%lu\n", m, n, sum1);

}