#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void setBit(char array[], int i, int value) // Totale Anzahl an Operationen: 14
{
    // Biteoperation, Wertezuweisung
    int byteOffset = i >> 3;
    // Biteoperation, Wertezuweisung
    int bitOffset = (i & 7);
    // 6 Bitoperationen, 2Indexzugriff, Wertezuweisung, Arithmethik
    array[byteOffset] = ((value & 1) << bitOffset) + (array[byteOffset] & ~(1 << bitOffset));
}

int getBit(char array[], int i) // Totale Anzahl an Opoerationen: 6
{
    // Indexzugriff, 4 Bitoperationen, Test
    return ((array[i >> 3] & 1 << (i & 7)) ? 1 : 0);
}

int main(int argc, char const *argv[])
{
    long unsigned int steps = 0;

    steps++; // Test
    if (argc != 2) {
        fprintf(stderr, "%s: Invalid number of arguments.\n", argv[0]);
        return EXIT_FAILURE;
    }

    int n;

    steps++; // Test
    if (sscanf(argv[1], "%d", &n) != 1 || n <= 0) {
        fprintf(stderr, "%s: Input is not a positive integer.\n", argv[0]);
        return EXIT_FAILURE;
    }

    //printf("%d\n", n);
    char * m;

    //printf("malloc size: %d\n", n / 8 + 1);
    steps += 2; // Wertezuweisung, malloc
    m = (char*) malloc(n / 8 + 1);
    
    steps += 14; // siehe setBit
    setBit(m, 0, 1);
    
    int i;
    steps++;// Wertezuweisung
    for (i = 1; i <= n; i++)
    {
        steps += 2; // Schleifenabruchsbedingung, Update
        steps += 2 + 6; // Arithmetik, Test, siehe getBit
        if (getBit(m, i - 1))
        {
            steps++; // printf
            printf("%d\n", i);
            steps += 3; // 2 Arithmetik, Test
            if (3*i +1 <= n)
            {
                steps += 1 + 14; // Arithmetik, siehe setBit
                setBit(m, 3*i, 1);
            }
            steps += 3; // 2 Arithmethik, Test
            if (2 * i + 1 <= n)
            {
                steps += 1 + 14; // Arithmetik, siehe setBit
                setBit(m, 2*i, 1);
            }
        }
    }

    steps++; // Freigabe des speichers
    free(m);

    printf("#%d\t%lu\n", n, steps);
    return EXIT_SUCCESS;
}
